use std::fs::File;
use std::io::Write;
use std::path::Path;

use thiserror::Error;

const INIT_CONFIG: &str = r#"---
tasks:
  shell_exec:
    shell/greet:
      description: print a greeting from the local shell
      commands:
        - echo "Hi!"
  docker_seq:
    docker/greet:
      description: print a greeting from within a container
      config:
        image: alpine:latest
        sequence:
          - commands:
              - echo "Hi!"
"#;

#[derive(Debug, Error)]
pub enum InitError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),

    #[error("AlreadyExistsError")]
    AlreadyExistsError,
}

pub fn init() -> Result<(), InitError> {
    let path = Path::new("ezd.yaml");
    if path.exists() {
        return Err(InitError::AlreadyExistsError);
    }
    let mut file = File::create(path)?;
    file.write_all(INIT_CONFIG.as_bytes())?;
    Ok(())
}
