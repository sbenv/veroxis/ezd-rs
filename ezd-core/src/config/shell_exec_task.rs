use std::collections::HashMap;

use colored::Colorize;
use serde::{Deserialize, Serialize};
use thiserror::Error;

use super::ezd::{find_config_path, FindConfigPathError};
use crate::shell::{execute, ShellError};

#[derive(Debug, Error)]
pub enum ShellExecTaskError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),

    #[error("ShellError: `{0}`")]
    ShellError(#[from] ShellError),

    #[error("FindConfigPathError: `{0}`")]
    FindConfigPathError(#[from] FindConfigPathError),

    #[error("ChangeProjectDirError")]
    ChangeProjectDirError,
}

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct ShellExecTask {
    pub env: Vec<String>,
    pub description: String,
    pub commands: Vec<String>,
}

impl ShellExecTask {
    pub fn exec(&self) -> Result<(), ShellExecTaskError> {
        let config_path = find_config_path()?;
        let config_dir = config_path
            .parent()
            .ok_or(ShellExecTaskError::ChangeProjectDirError)?;
        std::env::set_current_dir(config_dir)?;
        let envs: HashMap<String, String> = self
            .env
            .iter()
            .filter_map(|env_string| {
                let values: Vec<&str> = env_string.splitn(2, '=').collect();
                let key: &str = values.first().copied().unwrap_or("");
                let value: &str = values.get(1).copied().unwrap_or("");
                if key.is_empty() {
                    None
                } else {
                    Some((key.to_string(), value.to_string()))
                }
            })
            .collect();
        for cmd in self.commands.iter() {
            println!(
                "[{}:{}] {} {}",
                "ezd".green(),
                "shell_exec".green(),
                ">_".blue(),
                cmd.purple()
            );
            execute(cmd, &envs)?;
        }
        Ok(())
    }
}
