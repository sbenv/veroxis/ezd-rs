use std::collections::HashMap;
use std::path::PathBuf;

use colored::Colorize;
use serde::{Deserialize, Serialize};
use thiserror::Error;

use super::docker_seq_task::{DockerSeqTask, DockerSeqTaskError};
use super::env::{EzdEnvVars, EzdEnvVarsError};
use super::shell_exec_task::{ShellExecTask, ShellExecTaskError};
use super::volumes::{Volumes, VolumesError};

#[derive(Debug, Error)]
pub enum EzdError {
    #[error("ConfigParseError: `{0}`")]
    ConfigParseError(#[from] serde_yaml::Error),

    #[error("there are multiple tasks with the same name: {duplicate_tasks:?}")]
    TasksNotUnique { duplicate_tasks: Vec<String> },

    #[error("TaskDoesNotExistError: `{task}`")]
    TaskDoesNotExistError { task: String },

    #[error("ShellExecTaskError: `{0}`")]
    ShellExecTaskError(#[from] ShellExecTaskError),

    #[error("DockerSeqTaskError: `{0}`")]
    DockerSeqTaskError(#[from] DockerSeqTaskError),

    #[error("EzdEnvVarsError: `{0}`")]
    EzdEnvVarsError(#[from] EzdEnvVarsError),

    #[error("VolumesError: `{0}`")]
    VolumesError(#[from] VolumesError),
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Ezd {
    pub env: Vec<String>,
    pub tasks: Tasks,
    #[serde(flatten)]
    pub volumes: Volumes,
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Tasks {
    pub shell_exec: HashMap<String, ShellExecTask>,
    pub docker_seq: HashMap<String, DockerSeqTask>,
}

pub enum EzdTask {
    None,
    ShellExec(ShellExecTask),
    DockerSeq(Box<DockerSeqTask>),
}

impl TryFrom<String> for Ezd {
    type Error = EzdError;

    fn try_from(mut value: String) -> Result<Self, Self::Error> {
        value = Ezd::preprocess_config(value)?;
        match serde_yaml::from_str::<Ezd>(value.as_str()) {
            Ok(mut ezd) => {
                ezd.populate_env_fields()?;
                let duplicate_tasks = ezd.get_duplicate_tasks();
                if !duplicate_tasks.is_empty() {
                    return Err(EzdError::TasksNotUnique { duplicate_tasks });
                }
                Ok(ezd)
            }
            Err(err) => Err(EzdError::ConfigParseError(err)),
        }
    }
}

impl Ezd {
    pub fn run(&self, task_name: &str) -> Result<(), EzdError> {
        self.apply_volumes()?;
        if let Some(task) = self.tasks.shell_exec.get(task_name) {
            println!(
                "{prefix}{t}{suffix}",
                prefix = "[Executing shell_exec task: `".blue(),
                t = task_name.bright_blue().bold(),
                suffix = "`]".blue()
            );
            scopeguard::defer!(
                println!(
                    "{prefix}{t}{suffix}",
                    prefix="[Finished shell_exec task: `".blue(),
                    t=task_name.bright_blue().bold(),
                    suffix="`]".blue()
                );
            );
            task.exec()?;
            return Ok(());
        }
        if let Some(task) = self.tasks.docker_seq.get(task_name) {
            println!(
                "{prefix}{t}{suffix}",
                prefix = "[Executing docker_seq task: `".blue(),
                t = task_name.bright_blue().bold(),
                suffix = "`]".blue()
            );
            scopeguard::defer!(
                println!(
                    "{prefix}{t}{suffix}",
                    prefix="[Finished docker_seq task: `".blue(),
                    t=task_name.bright_blue().bold(),
                    suffix="`]".blue()
                );
            );
            task.exec()?;
            return Ok(());
        }
        Err(EzdError::TaskDoesNotExistError {
            task: task_name.into(),
        })
    }

    pub fn add_env(&mut self, key: String, value: String) {
        self.env.push(format!("{key}={value}"));
        self.populate_transitive_env_config();
    }

    pub fn preprocess_config(mut config: String) -> Result<String, EzdError> {
        let ezd_env_vars = EzdEnvVars::new()?;
        config = config.replace(
            "__EZD__",
            ezd_env_vars
                .ezd_executable
                .to_string_lossy()
                .to_string()
                .as_str(),
        );
        config = config.replace(
            "__EZD_HOST_PROJECT_ROOT__",
            ezd_env_vars
                .project_root
                .to_string_lossy()
                .to_string()
                .as_str(),
        );
        config = config.replace("__EZD_HOST_USERNAME__", ezd_env_vars.user_name.as_str());
        config = config.replace(
            "__EZD_HOST_UID__",
            ezd_env_vars.user_id.to_string().as_str(),
        );
        config = config.replace(
            "__EZD_HOST_GID__",
            ezd_env_vars.group_id.to_string().as_str(),
        );
        Ok(config)
    }

    pub fn populate_env_fields(&mut self) -> Result<(), EzdError> {
        let ezd_env_vars = EzdEnvVars::new()?;
        self.env.push(format!(
            "EZD={}",
            ezd_env_vars
                .ezd_executable
                .to_string_lossy()
                .to_string()
                .as_str(),
        ));
        self.env.push(format!(
            "EZD_HOST_PROJECT_ROOT={}",
            ezd_env_vars.project_root.to_string_lossy()
        ));
        self.env
            .push(format!("EZD_HOST_USERNAME={}", ezd_env_vars.user_name));
        self.env
            .push(format!("EZD_HOST_UID={}", ezd_env_vars.user_id));
        self.env
            .push(format!("EZD_HOST_GID={}", ezd_env_vars.group_id));
        self.populate_transitive_env_config();
        Ok(())
    }

    fn populate_transitive_env_config(&mut self) {
        for env in self.env.iter() {
            for (_, task) in self.tasks.shell_exec.iter_mut() {
                if !task.env.contains(env) {
                    task.env.push(env.clone());
                }
            }
            for (_, task) in self.tasks.docker_seq.iter_mut() {
                let config_env = task.config.env.get_or_insert(vec![]);
                if !config_env.contains(env) {
                    config_env.push(env.clone());
                }
            }
        }
    }

    pub fn get_tasks(&self) -> Vec<String> {
        let mut tasks = vec![];
        tasks.extend(self.tasks.shell_exec.keys().cloned());
        tasks.extend(self.tasks.docker_seq.keys().cloned());
        tasks.sort();
        tasks
    }

    pub fn get_duplicate_tasks(&self) -> Vec<String> {
        let mut tasks = vec![];
        let mut duplicate_tasks = vec![];
        tasks.extend(self.tasks.shell_exec.keys());
        tasks.extend(self.tasks.docker_seq.keys());
        tasks.sort();
        let mut last_task = String::new();
        for task in tasks.iter() {
            if last_task == **task {
                duplicate_tasks.push(last_task.clone());
            }
            last_task = (**task).clone();
        }
        duplicate_tasks.dedup();
        duplicate_tasks
    }

    pub fn get_task_by_name(&self, task_name: &str) -> EzdTask {
        for (name, task) in self.tasks.shell_exec.iter() {
            if name == task_name {
                return EzdTask::ShellExec((*task).clone());
            }
        }
        for (name, task) in self.tasks.docker_seq.iter() {
            if name == task_name {
                return EzdTask::DockerSeq(Box::new(task.clone()));
            }
        }
        EzdTask::None
    }

    pub fn apply_volumes(&self) -> Result<(), EzdError> {
        Ok(self.volumes.apply()?)
    }
}

#[derive(Debug, Error)]
pub enum FindConfigPathError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),
    #[error("can't find project path")]
    ProjectPathNotFoundError,
}

pub fn find_config_path() -> Result<PathBuf, FindConfigPathError> {
    let ezd_filenames = ["ezd.yaml", "ezd.json", "ezd.yml"];
    let current_dir = std::env::current_dir()?;
    for path in current_dir.ancestors() {
        for ezd_file in ezd_filenames.iter() {
            let mut path = PathBuf::from(path);
            path.push(ezd_file);
            if path.exists() {
                return Ok(path);
            }
        }
    }
    Err(FindConfigPathError::ProjectPathNotFoundError)
}
