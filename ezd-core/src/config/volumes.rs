use std::collections::HashMap;
use std::process::{Command, Stdio};
use std::string::FromUtf8Error;

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Volumes {
    #[serde(default)]
    volumes: HashMap<String, Option<Volume>>,
}

#[derive(Debug, Error)]
pub enum VolumesError {
    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),

    #[error("IoError: `{0}`")]
    FromUtf8Error(#[from] FromUtf8Error),

    #[error("VolumeCreateFailed: `{0}`")]
    VolumeCreateFailed(String),
}

impl Volumes {
    pub fn apply(&self) -> Result<(), VolumesError> {
        let existing_docker_volume_names = Volumes::query_existing_volumes()?;
        for (volume_name, volume) in self.volumes.iter() {
            if !existing_docker_volume_names.contains(volume_name) {
                Volumes::create(volume_name, volume)?;
            }
        }
        Ok(())
    }

    pub fn query_existing_volumes() -> Result<Vec<String>, VolumesError> {
        let output = Command::new("docker").args(vec!["volume", "ls"]).output()?;
        let stdout = String::from_utf8(output.stdout)?;
        Ok(stdout
            .split('\n')
            .skip(1)
            .filter(|line| !line.is_empty())
            .filter_map(|line| line.split_whitespace().nth(2).map(|word| word.to_string()))
            .collect())
    }

    pub fn create(volume_name: &str, volume: &Option<Volume>) -> Result<(), VolumesError> {
        let mut options: Vec<String> = vec![];
        if let Some(volume) = volume {
            if let Some(driver) = volume.driver.as_ref() {
                options.push("--driver".to_string());
                options.push(driver.clone());
            }
            if let Some(label) = volume.label.as_ref() {
                options.push("--label".to_string());
                options.push(label.clone());
            }
            if let Some(opts) = volume.opt.as_ref() {
                for opt in opts {
                    options.push("--opt".to_string());
                    options.push(opt.clone());
                }
            }
        }
        let result = Command::new("docker")
            .args(["volume", "create"])
            .args(&options)
            .arg(volume_name)
            .stderr(Stdio::null())
            .stdin(Stdio::null())
            .stdout(Stdio::null())
            .status()?;
        match result.success() {
            true => Ok(()),
            false => Err(VolumesError::VolumeCreateFailed(volume_name.into())),
        }
    }
}

#[derive(Debug, Default, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct Volume {
    pub driver: Option<String>,
    pub label: Option<String>,
    pub opt: Option<Vec<String>>,
}
