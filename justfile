check-all: cargo-check cargo-clippy cargo-test cargo-fmt

fix-all: cargo-fix-clippy cargo-fix-fmt

cargo-clean:
    cargo clean

cargo-check:
    cargo check

cargo-fmt:
    cargo fmt -- --check

cargo-fix-fmt:
    cargo +nightly fmt

cargo-clippy:
    cargo clippy --all-features -- -D warnings

cargo-fix-clippy:
    cargo clippy --fix --allow-dirty --allow-staged

cargo-test:
    cargo nextest run

build-release:
    cargo build --release

build-debug:
    cargo build

crossbuild-all: (crossbuild-release "x86_64-unknown-linux-gnu.2.17") (crossbuild-release "x86_64-unknown-linux-musl") (crossbuild-release "aarch64-unknown-linux-musl") (crossbuild-release "arm-unknown-linux-musleabihf")

crossbuild-release target:
    cargo zigbuild --release --target={{target}}

crossbuild-debug target:
    cargo zigbuild --target={{target}}

cargo-set-version version:
    cargo set-version --workspace {{version}}

nix command:
    nix --extra-experimental-features "nix-command flakes" develop --ignore-environment --command -- {{command}}
